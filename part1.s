# USER GUIDE
# To run this program, assemble it with GCC and enter your name as a parameter.  The program will then tell you your score.
# To exploit this program, locate the address of the variable sumOf by running this bash command: "objdump {fileName} -t | grep sumOf | cut 1-8"
#  and run the program with your name as "$(printf "\xyy\xyy\xyy\xyy")%08x.%08x.%08x.%08x.%08x.%150x.%n", replacing the Y's in printf with the address given by the bash command in little endian representation.
# The program will then give a score of 100 every time.

# To be able to win with your name as an input you need to set the clock to the right time and running the program in the same command, this can be done with 
# sudo date -s "$date --date='@22430226')" && ./{fuleName} {userName}
.section .data
toFormat:
	.asciz ", your number is... %d"
suffix:
	.asciz ".5"
name:
	.string "%s"
upper:
	.long 99
sumOf:
	.int 0
.section .bss
.section .text
.globl main
main:
#-12 random number 1
#-16 random number 2
#-20 sum of both random numbers
#-24 (sum/2)
#-92 the copy of argument [1]
#sets up stack
	leal 4(%esp), %ecx
	andl $-16, %esp
	pushl -4(%ecx)
	pushl %ebp
	movl %esp, %ebp
	pushl %ecx
	subl $100, %esp # space for the name 
	movl %ecx, %eax
	
	#copies argument [1] to -540
	movl 4(%eax), %eax 
	addl $4, %eax
	movl (%eax), %eax
	subl $8, %esp
	pushl %eax
	leal -92(%ebp), %eax
	pushl %eax
	call strcpy
	addl $16, %esp
	
	#Calls time and leaves it in the %eax
	subl $12, %esp
	pushl $0
	call time
	addl $16, %esp
	
	#uses the eax (the time) to seed SRAND
	subl $12, %esp
	pushl %eax
	call srand
	addl $16, %esp
	
	#calls the sub routine to get a random number and store it in the stack
	call _hundredRand
	movl %eax, -12(%ebp)
	call _hundredRand
	movl %eax, -16(%ebp)
	
	#moves the second random number to eax then add the first random number to it and saves it in -20
	movl -16(%ebp), %eax
	addl -12(%ebp), %eax
	movl %eax, sumOf
	
	#pushes the input string address to %eax and calls printf
	subl $12, %esp
	leal -92(%ebp), %eax
	pushl %eax
	call printf
	addl $16, %esp
	
	#shifts all bits right which is equal to divide by 2
	movl sumOf, %eax
	shrl %eax
	movl %eax, -24(%ebp)
	
	#uses and with a 1 to show which numbers are off or even 
	movl sumOf, %eax
	andl $1, %eax
	movl %eax, -28(%ebp)
	
	#pushes the halved sum to the stack, then the stringToFormat
	subl $8, %esp
	pushl -24(%ebp)
	pushl $toFormat
	call printf
	addl $16, %esp
	
	#-28 leaves 1 if odd and 0 if even, if even it jumps to the end
	cmpl $0, -28(%ebp)
	je .END
	
	#prints ".5" for if there is an odd number
	subl $12, %esp
	pushl $suffix
	call printf
	addl $16, %esp
	
	.END:
	subl $12, %esp	
	pushl $10  		# \n
	call putchar
	addl $16, %esp
	pushl $0
	call exit
	
	_hundredRand:
	pushl %ebp
	movl %esp, %ebp
	subl $8, %esp
	call rand
	movl upper, %ecx
	cltd
	idivl %ecx
	movl %edx, %eax
	addl $1, %eax
	movl %ebp, %esp
	popl %ebp
	ret