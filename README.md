# Neil Crosswaite For Esri

## Computer Architecture and Low Level Programming Coursework

This coursework was done as a pair, but there were two halves so what I am uploading is the work I took the lead on. I did all the coding and my team member then worked with me to get the exploits working. 
The assignment was to write a program for a 32-bit Ubuntu computer where to user inputted a name and got a score out of 100 which was the average of two random numbers 1-100.
We got 80% for the whole coursework.
The we had to be able to force the game to give us a score of 100. 
This means that there are errors in the code which I will explain now.

### printf

The first vulnerability is due to the use of a printf where the only argument is a variable. 

This means that for each % it find in the inputted string it takes an item off the stack. 

With the use of 

`$(printf "\x55\xa0\x04\x08")%08x.%08x.%08x.%08x.%08x.%150x.%n`

I am able to write to the memory address storing the total score. The order of the code had to be changed to allow writing to the total score.
This can be fixed by checking code doesn’t use printf(variable). This can be easily checked for even in a disassembled binary.
### Bad Randomness

This code is also weak due to bad use of a predictable random number.

A maximum score can be achieved by inputting

`sudo date -s "$(date --date='22430226')" && ./task1 Neil`

This makes use of the srand input being the time.
I was pleased with this exploit as it meant you could win with your own name as not many parents call their children printf<sup>[citation needed]</sup>
There are many better approaches to randomness than what I used here,
## Improving the code

These are some of the none security ways I have improved the code.

### How Does it Compile?

With the use of Godbolt I learnt that to find if a number is odd or even it used `AND 1` rather than the typed % 2. This lead me to explore how the same three functions in C compile. 

```
int modOdd(int num) 
{
    if (num % 2 == 1)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

int betterModOdd(int num)
{
    return (num % 2);
}

int andOdd(int num) 
{
    return (num & 1);
}
```
Although the compiler doesn't do the modulo there are still more steps in the first and second function than the third. so in a loop this could be vital. I aim to learn more like this.

### No need to float

As I only had to display the number after halving it, if the total was an odd number it added the suffix ".5"  rather than having to deal with a decimal number.


## Where Next

This piece of coursework was a great learning experience for me and I am eager to learn more and improve my understanding of the area with more practice.
